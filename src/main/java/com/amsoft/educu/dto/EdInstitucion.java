package com.amsoft.educu.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author JOAO
 */
@Entity
@Table(name = "ed_institucion")
@XmlRootElement
public class EdInstitucion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "inst_id")
    private Integer instId;
    @Basic(optional = false)
    @Column(name = "inst_tipoeduc")
    private String instTipoeduc;
    @Basic(optional = false)
    @Column(name = "inst_nombre")
    private String instNombre;
    @Column(name = "inst_mision")
    private String instMision;
    @Column(name = "inst_vision")
    private String instVision;
    @Column(name = "inst_aniofund")
    private String instAniofund;
    @Basic(optional = false)
    @Column(name = "inst_creacion")
    @Temporal(TemporalType.DATE)
    private Date instCreacion;
    @Column(name = "inst_email")
    private String instEmail;
    @Column(name = "inst_provincia")
    private String instProvincia;
    @Column(name = "inst_ciudad")
    private String instCiudad;
    @Column(name = "inst_direccion")
    private String instDireccion;
    @Column(name = "inst_observacion")
    private String instObservacion;
    @Column(name = "inst_telefono")
    private String instTelefono;
    @Column(name = "inst_celular")
    private String instCelular;
    @Basic(optional = false)
    @Column(name = "inst_estado")
    private Character instEstado;
    @OneToMany(mappedBy = "instId")
    private List<EdMallaporinstituto> edMallaporinstitutoList;
    @OneToMany(mappedBy = "instId")
    private List<EdMatricula> edMatriculaList;

    public EdInstitucion() {
    }

    public EdInstitucion(Integer instId) {
        this.instId = instId;
    }

    public EdInstitucion(Integer instId, String instTipoeduc, String instNombre, Date instCreacion, Character instEstado) {
        this.instId = instId;
        this.instTipoeduc = instTipoeduc;
        this.instNombre = instNombre;
        this.instCreacion = instCreacion;
        this.instEstado = instEstado;
    }

    public Integer getInstId() {
        return instId;
    }

    public void setInstId(Integer instId) {
        this.instId = instId;
    }

    public String getInstTipoeduc() {
        return instTipoeduc;
    }

    public void setInstTipoeduc(String instTipoeduc) {
        this.instTipoeduc = instTipoeduc;
    }

    public String getInstNombre() {
        return instNombre;
    }

    public void setInstNombre(String instNombre) {
        this.instNombre = instNombre;
    }

    public String getInstMision() {
        return instMision;
    }

    public void setInstMision(String instMision) {
        this.instMision = instMision;
    }

    public String getInstVision() {
        return instVision;
    }

    public void setInstVision(String instVision) {
        this.instVision = instVision;
    }

    public String getInstAniofund() {
        return instAniofund;
    }

    public void setInstAniofund(String instAniofund) {
        this.instAniofund = instAniofund;
    }

    public Date getInstCreacion() {
        return instCreacion;
    }

    public void setInstCreacion(Date instCreacion) {
        this.instCreacion = instCreacion;
    }

    public String getInstEmail() {
        return instEmail;
    }

    public void setInstEmail(String instEmail) {
        this.instEmail = instEmail;
    }

    public String getInstProvincia() {
        return instProvincia;
    }

    public void setInstProvincia(String instProvincia) {
        this.instProvincia = instProvincia;
    }

    public String getInstCiudad() {
        return instCiudad;
    }

    public void setInstCiudad(String instCiudad) {
        this.instCiudad = instCiudad;
    }

    public String getInstDireccion() {
        return instDireccion;
    }

    public void setInstDireccion(String instDireccion) {
        this.instDireccion = instDireccion;
    }

    public String getInstObservacion() {
        return instObservacion;
    }

    public void setInstObservacion(String instObservacion) {
        this.instObservacion = instObservacion;
    }

    public String getInstTelefono() {
        return instTelefono;
    }

    public void setInstTelefono(String instTelefono) {
        this.instTelefono = instTelefono;
    }

    public String getInstCelular() {
        return instCelular;
    }

    public void setInstCelular(String instCelular) {
        this.instCelular = instCelular;
    }

    public Character getInstEstado() {
        return instEstado;
    }

    public void setInstEstado(Character instEstado) {
        this.instEstado = instEstado;
    }

    @XmlTransient
    public List<EdMallaporinstituto> getEdMallaporinstitutoList() {
        return edMallaporinstitutoList;
    }

    public void setEdMallaporinstitutoList(List<EdMallaporinstituto> edMallaporinstitutoList) {
        this.edMallaporinstitutoList = edMallaporinstitutoList;
    }

    @XmlTransient
    public List<EdMatricula> getEdMatriculaList() {
        return edMatriculaList;
    }

    public void setEdMatriculaList(List<EdMatricula> edMatriculaList) {
        this.edMatriculaList = edMatriculaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (instId != null ? instId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EdInstitucion)) {
            return false;
        }
        EdInstitucion other = (EdInstitucion) object;
        if ((this.instId == null && other.instId != null) || (this.instId != null && !this.instId.equals(other.instId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "prueba_dos.EdInstitucion[ instId=" + instId + " ]";
    }
    
}
