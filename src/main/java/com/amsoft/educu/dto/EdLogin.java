package com.amsoft.educu.dto;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author JOAO
 */
@Entity
@Table(name = "ed_login")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EdLogin.findAll", query = "SELECT e FROM EdLogin e")
    , @NamedQuery(name = "EdLogin.findByLogiId", query = "SELECT e FROM EdLogin e WHERE e.logiId = :logiId")
    , @NamedQuery(name = "EdLogin.findByLogiUsuario", query = "SELECT e FROM EdLogin e WHERE e.logiUsuario = :logiUsuario")
    , @NamedQuery(name = "EdLogin.findByLogiEmail", query = "SELECT e FROM EdLogin e WHERE e.logiEmail = :logiEmail")
    , @NamedQuery(name = "EdLogin.findByLogiPassword", query = "SELECT e FROM EdLogin e WHERE e.logiPassword = :logiPassword")
    , @NamedQuery(name = "EdLogin.findByLogiEstado", query = "SELECT e FROM EdLogin e WHERE e.logiEstado = :logiEstado")
    , @NamedQuery(name = "EdLogin.findByLogiCreacion", query = "SELECT e FROM EdLogin e WHERE e.logiCreacion = :logiCreacion")})
public class EdLogin implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "logi_id")
    private Integer logiId;
    @Basic(optional = false)
    @Column(name = "logi_usuario")
    private String logiUsuario;
    @Basic(optional = false)
    @Column(name = "logi_email")
    private String logiEmail;
    @Basic(optional = false)
    @Column(name = "logi_password")
    private String logiPassword;
    @Basic(optional = false)
    @Column(name = "logi_estado")
    private Character logiEstado;
    @Basic(optional = false)
    @Column(name = "logi_creacion")
    @Temporal(TemporalType.DATE)
    private Date logiCreacion;

    public EdLogin() {
    }

    public EdLogin(Integer logiId) {
        this.logiId = logiId;
    }

    public EdLogin(Integer logiId, String logiUsuario, String logiEmail, String logiPassword, Character logiEstado, Date logiCreacion) {
        this.logiId = logiId;
        this.logiUsuario = logiUsuario;
        this.logiEmail = logiEmail;
        this.logiPassword = logiPassword;
        this.logiEstado = logiEstado;
        this.logiCreacion = logiCreacion;
    }

    public Integer getLogiId() {
        return logiId;
    }

    public void setLogiId(Integer logiId) {
        this.logiId = logiId;
    }

    public String getLogiUsuario() {
        return logiUsuario;
    }

    public void setLogiUsuario(String logiUsuario) {
        this.logiUsuario = logiUsuario;
    }

    public String getLogiEmail() {
        return logiEmail;
    }

    public void setLogiEmail(String logiEmail) {
        this.logiEmail = logiEmail;
    }

    public String getLogiPassword() {
        return logiPassword;
    }

    public void setLogiPassword(String logiPassword) {
        this.logiPassword = logiPassword;
    }

    public Character getLogiEstado() {
        return logiEstado;
    }

    public void setLogiEstado(Character logiEstado) {
        this.logiEstado = logiEstado;
    }

    public Date getLogiCreacion() {
        return logiCreacion;
    }

    public void setLogiCreacion(Date logiCreacion) {
        this.logiCreacion = logiCreacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (logiId != null ? logiId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EdLogin)) {
            return false;
        }
        EdLogin other = (EdLogin) object;
        if ((this.logiId == null && other.logiId != null) || (this.logiId != null && !this.logiId.equals(other.logiId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "prueba_dos.EdLogin[ logiId=" + logiId + " ]";
    }
    
}
