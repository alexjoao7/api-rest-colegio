package com.amsoft.educu.dto;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author JOAO
 */
@Entity
@Table(name = "ed_mallacurricular")
@XmlRootElement
public class EdMallacurricular implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "mallcurr_id")
	private Integer mallcurrId;
	@Basic(optional = false)
	@Column(name = "mallcurr_aniolect")
	private String mallcurrAniolect;
	@Basic(optional = false)
	@Column(name = "mallcurr_tipoeduc")
	private String mallcurrTipoeduc;
	@Basic(optional = false)
	@Column(name = "mallcurr_estado")
	private Character mallcurrEstado;
	@OneToMany(mappedBy = "mallcurrId")
	private List<EdMallaporinstituto> edMallaporinstitutoList;
	@OneToMany(mappedBy = "mallcurrId")
	private List<EdMateriasmalla> edMateriasmallaList;

	public EdMallacurricular() {
	}

	public EdMallacurricular(Integer mallcurrId) {
		this.mallcurrId = mallcurrId;
	}

	public EdMallacurricular(Integer mallcurrId, String mallcurrAniolect, String mallcurrTipoeduc,
			Character mallcurrEstado) {
		this.mallcurrId = mallcurrId;
		this.mallcurrAniolect = mallcurrAniolect;
		this.mallcurrTipoeduc = mallcurrTipoeduc;
		this.mallcurrEstado = mallcurrEstado;
	}

	public Integer getMallcurrId() {
		return mallcurrId;
	}

	public void setMallcurrId(Integer mallcurrId) {
		this.mallcurrId = mallcurrId;
	}

	public String getMallcurrAniolect() {
		return mallcurrAniolect;
	}

	public void setMallcurrAniolect(String mallcurrAniolect) {
		this.mallcurrAniolect = mallcurrAniolect;
	}

	public String getMallcurrTipoeduc() {
		return mallcurrTipoeduc;
	}

	public void setMallcurrTipoeduc(String mallcurrTipoeduc) {
		this.mallcurrTipoeduc = mallcurrTipoeduc;
	}

	public Character getMallcurrEstado() {
		return mallcurrEstado;
	}

	public void setMallcurrEstado(Character mallcurrEstado) {
		this.mallcurrEstado = mallcurrEstado;
	}

	@XmlTransient
	public List<EdMallaporinstituto> getEdMallaporinstitutoList() {
		return edMallaporinstitutoList;
	}

	public void setEdMallaporinstitutoList(List<EdMallaporinstituto> edMallaporinstitutoList) {
		this.edMallaporinstitutoList = edMallaporinstitutoList;
	}

	@XmlTransient
	public List<EdMateriasmalla> getEdMateriasmallaList() {
		return edMateriasmallaList;
	}

	public void setEdMateriasmallaList(List<EdMateriasmalla> edMateriasmallaList) {
		this.edMateriasmallaList = edMateriasmallaList;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (mallcurrId != null ? mallcurrId.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof EdMallacurricular)) {
			return false;
		}
		EdMallacurricular other = (EdMallacurricular) object;
		if ((this.mallcurrId == null && other.mallcurrId != null)
				|| (this.mallcurrId != null && !this.mallcurrId.equals(other.mallcurrId))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "prueba_dos.EdMallacurricular[ mallcurrId=" + mallcurrId + " ]";
	}

}
