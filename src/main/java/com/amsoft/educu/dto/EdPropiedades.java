package com.amsoft.educu.dto;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 *
 * @author JOAO
 */
@Entity
@Table(name = "ed_propiedades")
@XmlRootElement
public class EdPropiedades implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "prop_id")
	private Integer propId;
	@Basic(optional = false)
	@Column(name = "prop_nombre")
	private String propNombre;
	@Column(name = "prop_valor")
	private String propValor;
	@Column(name = "prop_observacion")
	private String propObservacion;
	@Basic(optional = false)
	@Column(name = "prop_estado")
	private Character propEstado;
	@OneToMany(mappedBy = "edPropId")
	private List<EdPropiedades> edPropiedadesList;
	@JoinColumn(name = "ed__prop_id", referencedColumnName = "prop_id")
	@JsonBackReference
	@ManyToOne
	private EdPropiedades edPropId;

	public EdPropiedades() {
	}

	public EdPropiedades(Integer propId) {
		this.propId = propId;
	}

	public EdPropiedades(Integer propId, String propNombre, Character propEstado) {
		this.propId = propId;
		this.propNombre = propNombre;
		this.propEstado = propEstado;
	}

	public Integer getPropId() {
		return propId;
	}

	public void setPropId(Integer propId) {
		this.propId = propId;
	}

	public String getPropNombre() {
		return propNombre;
	}

	public void setPropNombre(String propNombre) {
		this.propNombre = propNombre;
	}

	public String getPropValor() {
		return propValor;
	}

	public void setPropValor(String propValor) {
		this.propValor = propValor;
	}

	public String getPropObservacion() {
		return propObservacion;
	}

	public void setPropObservacion(String propObservacion) {
		this.propObservacion = propObservacion;
	}

	public Character getPropEstado() {
		return propEstado;
	}

	public void setPropEstado(Character propEstado) {
		this.propEstado = propEstado;
	}

	@XmlTransient
	public List<EdPropiedades> getEdPropiedadesList() {
		return edPropiedadesList;
	}

	public void setEdPropiedadesList(List<EdPropiedades> edPropiedadesList) {
		this.edPropiedadesList = edPropiedadesList;
	}

	@XmlTransient
	public EdPropiedades getEdPropId() {
		return edPropId;
	}

	public void setEdPropId(EdPropiedades edPropId) {
		this.edPropId = edPropId;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (propId != null ? propId.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof EdPropiedades)) {
			return false;
		}
		EdPropiedades other = (EdPropiedades) object;
		if ((this.propId == null && other.propId != null)
				|| (this.propId != null && !this.propId.equals(other.propId))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "prueba_dos.EdPropiedades[ propId=" + propId + " ]";
	}

}
