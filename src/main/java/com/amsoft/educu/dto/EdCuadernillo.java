package com.amsoft.educu.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author JOAO
 */
@Entity
@Table(name = "ed_cuadernillo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EdCuadernillo.findAll", query = "SELECT e FROM EdCuadernillo e")
    , @NamedQuery(name = "EdCuadernillo.findByCuadId", query = "SELECT e FROM EdCuadernillo e WHERE e.cuadId = :cuadId")
    , @NamedQuery(name = "EdCuadernillo.findByCuadParcial", query = "SELECT e FROM EdCuadernillo e WHERE e.cuadParcial = :cuadParcial")
    , @NamedQuery(name = "EdCuadernillo.findByCuadTrabindiv", query = "SELECT e FROM EdCuadernillo e WHERE e.cuadTrabindiv = :cuadTrabindiv")
    , @NamedQuery(name = "EdCuadernillo.findByCuadTrabgrup", query = "SELECT e FROM EdCuadernillo e WHERE e.cuadTrabgrup = :cuadTrabgrup")
    , @NamedQuery(name = "EdCuadernillo.findByCuadTrablecc", query = "SELECT e FROM EdCuadernillo e WHERE e.cuadTrablecc = :cuadTrablecc")
    , @NamedQuery(name = "EdCuadernillo.findByCuadExamen", query = "SELECT e FROM EdCuadernillo e WHERE e.cuadExamen = :cuadExamen")
    , @NamedQuery(name = "EdCuadernillo.findByCuadRecupuno", query = "SELECT e FROM EdCuadernillo e WHERE e.cuadRecupuno = :cuadRecupuno")
    , @NamedQuery(name = "EdCuadernillo.findByCuadRecupdos", query = "SELECT e FROM EdCuadernillo e WHERE e.cuadRecupdos = :cuadRecupdos")
    , @NamedQuery(name = "EdCuadernillo.findByCuadRecuptres", query = "SELECT e FROM EdCuadernillo e WHERE e.cuadRecuptres = :cuadRecuptres")
    , @NamedQuery(name = "EdCuadernillo.findByCuadRecucuatro", query = "SELECT e FROM EdCuadernillo e WHERE e.cuadRecucuatro = :cuadRecucuatro")})
public class EdCuadernillo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cuad_id")
    private Integer cuadId;
    @Basic(optional = false)
    @Column(name = "cuad_parcial")
    private String cuadParcial;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "cuad_trabindiv")
    private BigDecimal cuadTrabindiv;
    @Column(name = "cuad_trabgrup")
    private BigDecimal cuadTrabgrup;
    @Column(name = "cuad_trablecc")
    private BigDecimal cuadTrablecc;
    @Column(name = "cuad_examen")
    private BigDecimal cuadExamen;
    @Column(name = "cuad_recupuno")
    private BigDecimal cuadRecupuno;
    @Column(name = "cuad_recupdos")
    private BigDecimal cuadRecupdos;
    @Column(name = "cuad_recuptres")
    private BigDecimal cuadRecuptres;
    @Column(name = "cuad_recucuatro")
    private BigDecimal cuadRecucuatro;
    @JoinColumn(name = "usua_id", referencedColumnName = "usua_id")
    @ManyToOne
    private EdUsuario usuaId;

    public EdCuadernillo() {
    }

    public EdCuadernillo(Integer cuadId) {
        this.cuadId = cuadId;
    }

    public EdCuadernillo(Integer cuadId, String cuadParcial) {
        this.cuadId = cuadId;
        this.cuadParcial = cuadParcial;
    }

    public Integer getCuadId() {
        return cuadId;
    }

    public void setCuadId(Integer cuadId) {
        this.cuadId = cuadId;
    }

    public String getCuadParcial() {
        return cuadParcial;
    }

    public void setCuadParcial(String cuadParcial) {
        this.cuadParcial = cuadParcial;
    }

    public BigDecimal getCuadTrabindiv() {
        return cuadTrabindiv;
    }

    public void setCuadTrabindiv(BigDecimal cuadTrabindiv) {
        this.cuadTrabindiv = cuadTrabindiv;
    }

    public BigDecimal getCuadTrabgrup() {
        return cuadTrabgrup;
    }

    public void setCuadTrabgrup(BigDecimal cuadTrabgrup) {
        this.cuadTrabgrup = cuadTrabgrup;
    }

    public BigDecimal getCuadTrablecc() {
        return cuadTrablecc;
    }

    public void setCuadTrablecc(BigDecimal cuadTrablecc) {
        this.cuadTrablecc = cuadTrablecc;
    }

    public BigDecimal getCuadExamen() {
        return cuadExamen;
    }

    public void setCuadExamen(BigDecimal cuadExamen) {
        this.cuadExamen = cuadExamen;
    }

    public BigDecimal getCuadRecupuno() {
        return cuadRecupuno;
    }

    public void setCuadRecupuno(BigDecimal cuadRecupuno) {
        this.cuadRecupuno = cuadRecupuno;
    }

    public BigDecimal getCuadRecupdos() {
        return cuadRecupdos;
    }

    public void setCuadRecupdos(BigDecimal cuadRecupdos) {
        this.cuadRecupdos = cuadRecupdos;
    }

    public BigDecimal getCuadRecuptres() {
        return cuadRecuptres;
    }

    public void setCuadRecuptres(BigDecimal cuadRecuptres) {
        this.cuadRecuptres = cuadRecuptres;
    }

    public BigDecimal getCuadRecucuatro() {
        return cuadRecucuatro;
    }

    public void setCuadRecucuatro(BigDecimal cuadRecucuatro) {
        this.cuadRecucuatro = cuadRecucuatro;
    }

    public EdUsuario getUsuaId() {
        return usuaId;
    }

    public void setUsuaId(EdUsuario usuaId) {
        this.usuaId = usuaId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cuadId != null ? cuadId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EdCuadernillo)) {
            return false;
        }
        EdCuadernillo other = (EdCuadernillo) object;
        if ((this.cuadId == null && other.cuadId != null) || (this.cuadId != null && !this.cuadId.equals(other.cuadId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "prueba_dos.EdCuadernillo[ cuadId=" + cuadId + " ]";
    }
    
}