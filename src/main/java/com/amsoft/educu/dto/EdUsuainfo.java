package com.amsoft.educu.dto;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author JOAO
 */
@Entity
@Table(name = "ed_usuainfo")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "EdUsuainfo.findAll", query = "SELECT e FROM EdUsuainfo e"),
		@NamedQuery(name = "EdUsuainfo.findByUsuainfoId", query = "SELECT e FROM EdUsuainfo e WHERE e.usuainfoId = :usuainfoId"),
		@NamedQuery(name = "EdUsuainfo.findByUsuainfoHermanos", query = "SELECT e FROM EdUsuainfo e WHERE e.usuainfoHermanos = :usuainfoHermanos"),
		@NamedQuery(name = "EdUsuainfo.findByUsuainfoHermanas", query = "SELECT e FROM EdUsuainfo e WHERE e.usuainfoHermanas = :usuainfoHermanas"),
		@NamedQuery(name = "EdUsuainfo.findByUsuainfoPosicion", query = "SELECT e FROM EdUsuainfo e WHERE e.usuainfoPosicion = :usuainfoPosicion"),
		@NamedQuery(name = "EdUsuainfo.findByUsuainfoColeanter", query = "SELECT e FROM EdUsuainfo e WHERE e.usuainfoColeanter = :usuainfoColeanter") })
public class EdUsuainfo implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "usuainfo_id")
	private Integer usuainfoId;
	@Column(name = "usuainfo_hermanos")
	private String usuainfoHermanos;
	@Column(name = "usuainfo_hermanas")
	private String usuainfoHermanas;
	@Column(name = "usuainfo_posicion")
	private String usuainfoPosicion;
	@Column(name = "usuainfo_coleanter")
	private String usuainfoColeanter;
	@JoinColumn(name = "usua_id", referencedColumnName = "usua_id")
	@ManyToOne
	private EdUsuario usuaId;

	public EdUsuainfo() {
	}

	public EdUsuainfo(Integer usuainfoId) {
		this.usuainfoId = usuainfoId;
	}

	public Integer getUsuainfoId() {
		return usuainfoId;
	}

	public void setUsuainfoId(Integer usuainfoId) {
		this.usuainfoId = usuainfoId;
	}

	public String getUsuainfoHermanos() {
		return usuainfoHermanos;
	}

	public void setUsuainfoHermanos(String usuainfoHermanos) {
		this.usuainfoHermanos = usuainfoHermanos;
	}

	public String getUsuainfoHermanas() {
		return usuainfoHermanas;
	}

	public void setUsuainfoHermanas(String usuainfoHermanas) {
		this.usuainfoHermanas = usuainfoHermanas;
	}

	public String getUsuainfoPosicion() {
		return usuainfoPosicion;
	}

	public void setUsuainfoPosicion(String usuainfoPosicion) {
		this.usuainfoPosicion = usuainfoPosicion;
	}

	public String getUsuainfoColeanter() {
		return usuainfoColeanter;
	}

	public void setUsuainfoColeanter(String usuainfoColeanter) {
		this.usuainfoColeanter = usuainfoColeanter;
	}

	public EdUsuario getUsuaId() {
		return usuaId;
	}

	public void setUsuaId(EdUsuario usuaId) {
		this.usuaId = usuaId;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (usuainfoId != null ? usuainfoId.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof EdUsuainfo)) {
			return false;
		}
		EdUsuainfo other = (EdUsuainfo) object;
		if ((this.usuainfoId == null && other.usuainfoId != null)
				|| (this.usuainfoId != null && !this.usuainfoId.equals(other.usuainfoId))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "prueba_dos.EdUsuainfo[ usuainfoId=" + usuainfoId + " ]";
	}

}
