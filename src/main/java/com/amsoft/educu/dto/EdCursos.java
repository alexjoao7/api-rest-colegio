package com.amsoft.educu.dto;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author JOAO
 */
@Entity
@Table(name = "ed_cursos")
@XmlRootElement
public class EdCursos implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "curso_id")
	private Integer cursoId;
	@Basic(optional = false)
	@Column(name = "curso_grado")
	private String cursoGrado;
	@Basic(optional = false)
	@Column(name = "secccurs_paralelo")
	private String secccursParalelo;
	@Basic(optional = false)
	@Column(name = "curso_estado")
	private Character cursoEstado;
	@OneToMany(mappedBy = "cursoId")
	private List<EdMateriasporprofesor> edMateriasporprofesorList;
	@OneToMany(mappedBy = "cursoId")
	private List<EdMatricula> edMatriculaList;
	@OneToMany(mappedBy = "cursoId")
	private List<EdTareas> edTareasList;

	public EdCursos() {
	}

	public EdCursos(Integer cursoId) {
		this.cursoId = cursoId;
	}

	public EdCursos(Integer cursoId, String cursoGrado, String secccursParalelo, Character cursoEstado) {
		this.cursoId = cursoId;
		this.cursoGrado = cursoGrado;
		this.secccursParalelo = secccursParalelo;
		this.cursoEstado = cursoEstado;
	}

	public Integer getCursoId() {
		return cursoId;
	}

	public void setCursoId(Integer cursoId) {
		this.cursoId = cursoId;
	}

	public String getCursoGrado() {
		return cursoGrado;
	}

	public void setCursoGrado(String cursoGrado) {
		this.cursoGrado = cursoGrado;
	}

	public String getSecccursParalelo() {
		return secccursParalelo;
	}

	public void setSecccursParalelo(String secccursParalelo) {
		this.secccursParalelo = secccursParalelo;
	}

	public Character getCursoEstado() {
		return cursoEstado;
	}

	public void setCursoEstado(Character cursoEstado) {
		this.cursoEstado = cursoEstado;
	}

	@XmlTransient
	public List<EdMateriasporprofesor> getEdMateriasporprofesorList() {
		return edMateriasporprofesorList;
	}

	public void setEdMateriasporprofesorList(List<EdMateriasporprofesor> edMateriasporprofesorList) {
		this.edMateriasporprofesorList = edMateriasporprofesorList;
	}

	@XmlTransient
	public List<EdMatricula> getEdMatriculaList() {
		return edMatriculaList;
	}

	public void setEdMatriculaList(List<EdMatricula> edMatriculaList) {
		this.edMatriculaList = edMatriculaList;
	}

	@XmlTransient
	public List<EdTareas> getEdTareasList() {
		return edTareasList;
	}

	public void setEdTareasList(List<EdTareas> edTareasList) {
		this.edTareasList = edTareasList;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (cursoId != null ? cursoId.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof EdCursos)) {
			return false;
		}
		EdCursos other = (EdCursos) object;
		if ((this.cursoId == null && other.cursoId != null)
				|| (this.cursoId != null && !this.cursoId.equals(other.cursoId))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "prueba_dos.EdCursos[ cursoId=" + cursoId + " ]";
	}

}