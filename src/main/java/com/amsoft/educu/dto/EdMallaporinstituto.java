package com.amsoft.educu.dto;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author JOAO
 */
@Entity
@Table(name = "ed_mallaporinstituto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EdMallaporinstituto.findAll", query = "SELECT e FROM EdMallaporinstituto e")
    , @NamedQuery(name = "EdMallaporinstituto.findByMallinstId", query = "SELECT e FROM EdMallaporinstituto e WHERE e.mallinstId = :mallinstId")
    , @NamedQuery(name = "EdMallaporinstituto.findByMallinstEstado", query = "SELECT e FROM EdMallaporinstituto e WHERE e.mallinstEstado = :mallinstEstado")})
public class EdMallaporinstituto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "mallinst_id")
    private Integer mallinstId;
    @Basic(optional = false)
    @Column(name = "mallinst_estado")
    private Character mallinstEstado;
    @JoinColumn(name = "inst_id", referencedColumnName = "inst_id")
    @ManyToOne
    private EdInstitucion instId;
    @JoinColumn(name = "mallcurr_id", referencedColumnName = "mallcurr_id")
    @ManyToOne
    private EdMallacurricular mallcurrId;

    public EdMallaporinstituto() {
    }

    public EdMallaporinstituto(Integer mallinstId) {
        this.mallinstId = mallinstId;
    }

    public EdMallaporinstituto(Integer mallinstId, Character mallinstEstado) {
        this.mallinstId = mallinstId;
        this.mallinstEstado = mallinstEstado;
    }

    public Integer getMallinstId() {
        return mallinstId;
    }

    public void setMallinstId(Integer mallinstId) {
        this.mallinstId = mallinstId;
    }

    public Character getMallinstEstado() {
        return mallinstEstado;
    }

    public void setMallinstEstado(Character mallinstEstado) {
        this.mallinstEstado = mallinstEstado;
    }

    public EdInstitucion getInstId() {
        return instId;
    }

    public void setInstId(EdInstitucion instId) {
        this.instId = instId;
    }

    public EdMallacurricular getMallcurrId() {
        return mallcurrId;
    }

    public void setMallcurrId(EdMallacurricular mallcurrId) {
        this.mallcurrId = mallcurrId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (mallinstId != null ? mallinstId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EdMallaporinstituto)) {
            return false;
        }
        EdMallaporinstituto other = (EdMallaporinstituto) object;
        if ((this.mallinstId == null && other.mallinstId != null) || (this.mallinstId != null && !this.mallinstId.equals(other.mallinstId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "prueba_dos.EdMallaporinstituto[ mallinstId=" + mallinstId + " ]";
    }
    
}

