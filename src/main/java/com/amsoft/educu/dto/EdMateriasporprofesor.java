package com.amsoft.educu.dto;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author JOAO
 */
@Entity
@Table(name = "ed_materiasporprofesor")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EdMateriasporprofesor.findAll", query = "SELECT e FROM EdMateriasporprofesor e")
    , @NamedQuery(name = "EdMateriasporprofesor.findByMateprofId", query = "SELECT e FROM EdMateriasporprofesor e WHERE e.mateprofId = :mateprofId")
    , @NamedQuery(name = "EdMateriasporprofesor.findByMateprofFecha", query = "SELECT e FROM EdMateriasporprofesor e WHERE e.mateprofFecha = :mateprofFecha")
    , @NamedQuery(name = "EdMateriasporprofesor.findByMateprofEstado", query = "SELECT e FROM EdMateriasporprofesor e WHERE e.mateprofEstado = :mateprofEstado")})
public class EdMateriasporprofesor implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "mateprof_id")
    private Integer mateprofId;
    @Basic(optional = false)
    @Column(name = "mateprof_fecha")
    @Temporal(TemporalType.DATE)
    private Date mateprofFecha;
    @Basic(optional = false)
    @Column(name = "mateprof_estado")
    private Character mateprofEstado;
    @JoinColumn(name = "curso_id", referencedColumnName = "curso_id")
    @ManyToOne
    private EdCursos cursoId;
    @JoinColumn(name = "matemall_id", referencedColumnName = "matemall_id")
    @ManyToOne
    private EdMateriasmalla matemallId;
    @JoinColumn(name = "usua_id", referencedColumnName = "usua_id")
    @ManyToOne
    private EdUsuario usuaId;

    public EdMateriasporprofesor() {
    }

    public EdMateriasporprofesor(Integer mateprofId) {
        this.mateprofId = mateprofId;
    }

    public EdMateriasporprofesor(Integer mateprofId, Date mateprofFecha, Character mateprofEstado) {
        this.mateprofId = mateprofId;
        this.mateprofFecha = mateprofFecha;
        this.mateprofEstado = mateprofEstado;
    }

    public Integer getMateprofId() {
        return mateprofId;
    }

    public void setMateprofId(Integer mateprofId) {
        this.mateprofId = mateprofId;
    }

    public Date getMateprofFecha() {
        return mateprofFecha;
    }

    public void setMateprofFecha(Date mateprofFecha) {
        this.mateprofFecha = mateprofFecha;
    }

    public Character getMateprofEstado() {
        return mateprofEstado;
    }

    public void setMateprofEstado(Character mateprofEstado) {
        this.mateprofEstado = mateprofEstado;
    }

    public EdCursos getCursoId() {
        return cursoId;
    }

    public void setCursoId(EdCursos cursoId) {
        this.cursoId = cursoId;
    }

    public EdMateriasmalla getMatemallId() {
        return matemallId;
    }

    public void setMatemallId(EdMateriasmalla matemallId) {
        this.matemallId = matemallId;
    }

    public EdUsuario getUsuaId() {
        return usuaId;
    }

    public void setUsuaId(EdUsuario usuaId) {
        this.usuaId = usuaId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (mateprofId != null ? mateprofId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EdMateriasporprofesor)) {
            return false;
        }
        EdMateriasporprofesor other = (EdMateriasporprofesor) object;
        if ((this.mateprofId == null && other.mateprofId != null) || (this.mateprofId != null && !this.mateprofId.equals(other.mateprofId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "prueba_dos.EdMateriasporprofesor[ mateprofId=" + mateprofId + " ]";
    }
    
}
