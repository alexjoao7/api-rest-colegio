package com.amsoft.educu.dto;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author JOAO
 */
@Entity
@Table(name = "ed_fichamedica")
@XmlRootElement
public class EdFichaMedica implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "fichmedi_id")
	private Integer fichmediId;
	@Column(name = "fichmedi_altura")
	private String fichmediAltura;
	@Column(name = "fichmedi_peso")
	private String fichmediPeso;
	@Column(name = "fichmedi_tiposangre")
	private String fichmediTiposangre;
	@JoinColumn(name = "usua_id", referencedColumnName = "usua_id")
	@ManyToOne
	private EdUsuario usuaId;

	public EdFichaMedica() {
	}

	public EdFichaMedica(Integer fichmediId) {
		this.fichmediId = fichmediId;
	}

	public Integer getFichmediId() {
		return fichmediId;
	}

	public void setFichmediId(Integer fichmediId) {
		this.fichmediId = fichmediId;
	}

	public String getFichmediAltura() {
		return fichmediAltura;
	}

	public void setFichmediAltura(String fichmediAltura) {
		this.fichmediAltura = fichmediAltura;
	}

	public String getFichmediPeso() {
		return fichmediPeso;
	}

	public void setFichmediPeso(String fichmediPeso) {
		this.fichmediPeso = fichmediPeso;
	}

	public String getFichmediTiposangre() {
		return fichmediTiposangre;
	}

	public void setFichmediTiposangre(String fichmediTiposangre) {
		this.fichmediTiposangre = fichmediTiposangre;
	}

	public EdUsuario getUsuaId() {
		return usuaId;
	}

	public void setUsuaId(EdUsuario usuaId) {
		this.usuaId = usuaId;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (fichmediId != null ? fichmediId.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof EdFichaMedica)) {
			return false;
		}
		EdFichaMedica other = (EdFichaMedica) object;
		if ((this.fichmediId == null && other.fichmediId != null)
				|| (this.fichmediId != null && !this.fichmediId.equals(other.fichmediId))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "prueba_dos.EdFichamedica[ fichmediId=" + fichmediId + " ]";
	}

}