package com.amsoft.educu.dto;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author JOAO
 */
@Entity
@Table(name = "ed_materiasmalla")
@XmlRootElement
public class EdMateriasmalla implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "matemall_id")
	private Integer matemallId;
	@Basic(optional = false)
	@Column(name = "matemall_materia")
	private String matemallMateria;
	@Basic(optional = false)
	@Column(name = "matemall_grado")
	private String matemallGrado;
	@Basic(optional = false)
	@Column(name = "matemall_horas")
	private int matemallHoras;
	@OneToMany(mappedBy = "matemallId")
	private List<EdMateriasporprofesor> edMateriasporprofesorList;
	@OneToMany(mappedBy = "matemallId")
	private List<EdTareas> edTareasList;
	@JoinColumn(name = "mallcurr_id", referencedColumnName = "mallcurr_id")
	@ManyToOne
	private EdMallacurricular mallcurrId;

	public EdMateriasmalla() {
	}

	public EdMateriasmalla(Integer matemallId) {
		this.matemallId = matemallId;
	}

	public EdMateriasmalla(Integer matemallId, String matemallMateria, String matemallGrado, int matemallHoras) {
		this.matemallId = matemallId;
		this.matemallMateria = matemallMateria;
		this.matemallGrado = matemallGrado;
		this.matemallHoras = matemallHoras;
	}

	public Integer getMatemallId() {
		return matemallId;
	}

	public void setMatemallId(Integer matemallId) {
		this.matemallId = matemallId;
	}

	public String getMatemallMateria() {
		return matemallMateria;
	}

	public void setMatemallMateria(String matemallMateria) {
		this.matemallMateria = matemallMateria;
	}

	public String getMatemallGrado() {
		return matemallGrado;
	}

	public void setMatemallGrado(String matemallGrado) {
		this.matemallGrado = matemallGrado;
	}

	public int getMatemallHoras() {
		return matemallHoras;
	}

	public void setMatemallHoras(int matemallHoras) {
		this.matemallHoras = matemallHoras;
	}

	@XmlTransient
	public List<EdMateriasporprofesor> getEdMateriasporprofesorList() {
		return edMateriasporprofesorList;
	}

	public void setEdMateriasporprofesorList(List<EdMateriasporprofesor> edMateriasporprofesorList) {
		this.edMateriasporprofesorList = edMateriasporprofesorList;
	}

	@XmlTransient
	public List<EdTareas> getEdTareasList() {
		return edTareasList;
	}

	public void setEdTareasList(List<EdTareas> edTareasList) {
		this.edTareasList = edTareasList;
	}

	public EdMallacurricular getMallcurrId() {
		return mallcurrId;
	}

	public void setMallcurrId(EdMallacurricular mallcurrId) {
		this.mallcurrId = mallcurrId;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (matemallId != null ? matemallId.hashCode() : 0);
		return hash;
	}

	@Override
	public String toString() {
		return "prueba_dos.EdMateriasmalla[ matemallId=" + matemallId + " ]";
	}

}