package com.amsoft.educu.dto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "ed_matricula")
@XmlRootElement
public class EdMatricula implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "matr_id")
	private Integer matrId;
	@Column(name = "matr_fecha")
	@Temporal(TemporalType.DATE)
	private Date matrFecha;
	@Column(name = "matr_observacion")
	private String matrObservacion;
	@JoinColumn(name = "curso_id", referencedColumnName = "curso_id")
	@ManyToOne
	private EdCursos cursoId;
	@JoinColumn(name = "inst_id", referencedColumnName = "inst_id")
	@ManyToOne
	private EdInstitucion instId;
	@JoinColumn(name = "usua_id", referencedColumnName = "usua_id")
	@ManyToOne
	private EdUsuario usuaId;

	public EdMatricula() {
	}

	public EdMatricula(Integer matrId) {
		this.matrId = matrId;
	}

	public Integer getMatrId() {
		return matrId;
	}

	public void setMatrId(Integer matrId) {
		this.matrId = matrId;
	}

	public Date getMatrFecha() {
		return matrFecha;
	}

	public void setMatrFecha(Date matrFecha) {
		this.matrFecha = matrFecha;
	}

	public String getMatrObservacion() {
		return matrObservacion;
	}

	public void setMatrObservacion(String matrObservacion) {
		this.matrObservacion = matrObservacion;
	}

	public EdCursos getCursoId() {
		return cursoId;
	}

	public void setCursoId(EdCursos cursoId) {
		this.cursoId = cursoId;
	}

	public EdInstitucion getInstId() {
		return instId;
	}

	public void setInstId(EdInstitucion instId) {
		this.instId = instId;
	}

	public EdUsuario getUsuaId() {
		return usuaId;
	}

	public void setUsuaId(EdUsuario usuaId) {
		this.usuaId = usuaId;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (matrId != null ? matrId.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof EdMatricula)) {
			return false;
		}
		EdMatricula other = (EdMatricula) object;
		if ((this.matrId == null && other.matrId != null)
				|| (this.matrId != null && !this.matrId.equals(other.matrId))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "prueba_dos.EdMatricula[ matrId=" + matrId + " ]";
	}

}