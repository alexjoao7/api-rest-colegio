package com.amsoft.educu.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author JOAO
 */
@Entity
@Table(name = "ed_usuario")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "EdUsuario.findAll", query = "SELECT e FROM EdUsuario e"),
		@NamedQuery(name = "EdUsuario.findByUsuaId", query = "SELECT e FROM EdUsuario e WHERE e.usuaId = :usuaId"),
		@NamedQuery(name = "EdUsuario.findByUsuaTipoiden", query = "SELECT e FROM EdUsuario e WHERE e.usuaTipoiden = :usuaTipoiden"),
		@NamedQuery(name = "EdUsuario.findByUsuaIdentificacion", query = "SELECT e FROM EdUsuario e WHERE e.usuaIdentificacion = :usuaIdentificacion"),
		@NamedQuery(name = "EdUsuario.findByUsuaNombres", query = "SELECT e FROM EdUsuario e WHERE e.usuaNombres = :usuaNombres"),
		@NamedQuery(name = "EdUsuario.findByUsuaApellidos", query = "SELECT e FROM EdUsuario e WHERE e.usuaApellidos = :usuaApellidos"),
		@NamedQuery(name = "EdUsuario.findByUsuaEmail", query = "SELECT e FROM EdUsuario e WHERE e.usuaEmail = :usuaEmail"),
		@NamedQuery(name = "EdUsuario.findByUsuaCelu", query = "SELECT e FROM EdUsuario e WHERE e.usuaCelu = :usuaCelu"),
		@NamedQuery(name = "EdUsuario.findByUsuaImagen", query = "SELECT e FROM EdUsuario e WHERE e.usuaImagen = :usuaImagen"),
		@NamedQuery(name = "EdUsuario.findByUsuaFechnaci", query = "SELECT e FROM EdUsuario e WHERE e.usuaFechnaci = :usuaFechnaci"),
		@NamedQuery(name = "EdUsuario.findByUsuaGenero", query = "SELECT e FROM EdUsuario e WHERE e.usuaGenero = :usuaGenero"),
		@NamedQuery(name = "EdUsuario.findByUsuaProveedor", query = "SELECT e FROM EdUsuario e WHERE e.usuaProveedor = :usuaProveedor"),
		@NamedQuery(name = "EdUsuario.findByUsuaNacionalidad", query = "SELECT e FROM EdUsuario e WHERE e.usuaNacionalidad = :usuaNacionalidad"),
		@NamedQuery(name = "EdUsuario.findByUsuaLuganaci", query = "SELECT e FROM EdUsuario e WHERE e.usuaLuganaci = :usuaLuganaci"),
		@NamedQuery(name = "EdUsuario.findByUsuaCivil", query = "SELECT e FROM EdUsuario e WHERE e.usuaCivil = :usuaCivil"),
		@NamedQuery(name = "EdUsuario.findByUsuaNiveeduc", query = "SELECT e FROM EdUsuario e WHERE e.usuaNiveeduc = :usuaNiveeduc"),
		@NamedQuery(name = "EdUsuario.findByUsuaDiredomi", query = "SELECT e FROM EdUsuario e WHERE e.usuaDiredomi = :usuaDiredomi"),
		@NamedQuery(name = "EdUsuario.findByUsuaTeledomi", query = "SELECT e FROM EdUsuario e WHERE e.usuaTeledomi = :usuaTeledomi"),
		@NamedQuery(name = "EdUsuario.findByUsuaProfesion", query = "SELECT e FROM EdUsuario e WHERE e.usuaProfesion = :usuaProfesion"),
		@NamedQuery(name = "EdUsuario.findByUsuaOcupasion", query = "SELECT e FROM EdUsuario e WHERE e.usuaOcupasion = :usuaOcupasion"),
		@NamedQuery(name = "EdUsuario.findByUsuaEmprtrab", query = "SELECT e FROM EdUsuario e WHERE e.usuaEmprtrab = :usuaEmprtrab"),
		@NamedQuery(name = "EdUsuario.findByUsuaDiretrab", query = "SELECT e FROM EdUsuario e WHERE e.usuaDiretrab = :usuaDiretrab"),
		@NamedQuery(name = "EdUsuario.findByUsuaTeletrab", query = "SELECT e FROM EdUsuario e WHERE e.usuaTeletrab = :usuaTeletrab"),
		@NamedQuery(name = "EdUsuario.findByUsuaVivienda", query = "SELECT e FROM EdUsuario e WHERE e.usuaVivienda = :usuaVivienda"),
		@NamedQuery(name = "EdUsuario.findByUsuaEstado", query = "SELECT e FROM EdUsuario e WHERE e.usuaEstado = :usuaEstado"),
		@NamedQuery(name = "EdUsuario.findByUsuaCreacion", query = "SELECT e FROM EdUsuario e WHERE e.usuaCreacion = :usuaCreacion") })
public class EdUsuario implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "usua_id")
	private Integer usuaId;
	@Basic(optional = false)
	@Column(name = "usua_tipoiden")
	private Character usuaTipoiden;
	@Basic(optional = false)
	@Column(name = "usua_identificacion")
	private String usuaIdentificacion;
	@Basic(optional = false)
	@Column(name = "usua_nombres")
	private String usuaNombres;
	@Basic(optional = false)
	@Column(name = "usua_apellidos")
	private String usuaApellidos;
	@Basic(optional = false)
	@Column(name = "usua_email")
	private String usuaEmail;
	@Column(name = "usua_celu")
	private String usuaCelu;
	@Column(name = "usua_imagen")
	private String usuaImagen;
	@Column(name = "usua_fechnaci")
	@Temporal(TemporalType.DATE)
	private Date usuaFechnaci;
	@Column(name = "usua_genero")
	private Character usuaGenero;
	@Column(name = "usua_proveedor")
	private String usuaProveedor;
	@Column(name = "usua_nacionalidad")
	private String usuaNacionalidad;
	@Column(name = "usua_luganaci")
	private String usuaLuganaci;
	@Column(name = "usua_civil")
	private String usuaCivil;
	@Column(name = "usua_niveeduc")
	private String usuaNiveeduc;
	@Column(name = "usua_diredomi")
	private String usuaDiredomi;
	@Column(name = "usua_teledomi")
	private String usuaTeledomi;
	@Column(name = "usua_profesion")
	private String usuaProfesion;
	@Column(name = "usua_ocupasion")
	private String usuaOcupasion;
	@Column(name = "usua_emprtrab")
	private String usuaEmprtrab;
	@Column(name = "usua_diretrab")
	private String usuaDiretrab;
	@Column(name = "usua_teletrab")
	private String usuaTeletrab;
	@Column(name = "usua_vivienda")
	private String usuaVivienda;
	@Basic(optional = false)
	@Column(name = "usua_estado")
	private Character usuaEstado;
	@Basic(optional = false)
	@Column(name = "usua_creacion")
	@Temporal(TemporalType.DATE)
	private Date usuaCreacion;
	@ManyToMany(mappedBy = "edUsuarioList")
	private List<EdPerfil> edPerfilList;
	@OneToMany(mappedBy = "usuaId")
	private List<EdLibretacalificaciones> edLibretacalificacionesList;
	@OneToMany(mappedBy = "usuaId")
	private List<EdMateriasporprofesor> edMateriasporprofesorList;
	@OneToMany(mappedBy = "usuaId")
	private List<EdMatricula> edMatriculaList;
	@OneToMany(mappedBy = "usuaId")
	private List<EdCuadernillo> edCuadernilloList;
	@OneToMany(mappedBy = "usuaId")
	private List<EdUsuainfo> edUsuainfoList;
	@OneToMany(mappedBy = "usuaId")
	private List<EdFichaMedica> edFichamedicaList;
	@OneToMany(mappedBy = "edUsuaId")
	private List<EdUsuario> edUsuarioList;
	@JoinColumn(name = "ed__usua_id", referencedColumnName = "usua_id")
	@ManyToOne
	private EdUsuario edUsuaId;

	public EdUsuario() {
	}

	public EdUsuario(Integer usuaId) {
		this.usuaId = usuaId;
	}

	public EdUsuario(Integer usuaId, Character usuaTipoiden, String usuaIdentificacion, String usuaNombres,
			String usuaApellidos, String usuaEmail, Character usuaEstado, Date usuaCreacion) {
		this.usuaId = usuaId;
		this.usuaTipoiden = usuaTipoiden;
		this.usuaIdentificacion = usuaIdentificacion;
		this.usuaNombres = usuaNombres;
		this.usuaApellidos = usuaApellidos;
		this.usuaEmail = usuaEmail;
		this.usuaEstado = usuaEstado;
		this.usuaCreacion = usuaCreacion;
	}

	public Integer getUsuaId() {
		return usuaId;
	}

	public void setUsuaId(Integer usuaId) {
		this.usuaId = usuaId;
	}

	public Character getUsuaTipoiden() {
		return usuaTipoiden;
	}

	public void setUsuaTipoiden(Character usuaTipoiden) {
		this.usuaTipoiden = usuaTipoiden;
	}

	public String getUsuaIdentificacion() {
		return usuaIdentificacion;
	}

	public void setUsuaIdentificacion(String usuaIdentificacion) {
		this.usuaIdentificacion = usuaIdentificacion;
	}

	public String getUsuaNombres() {
		return usuaNombres;
	}

	public void setUsuaNombres(String usuaNombres) {
		this.usuaNombres = usuaNombres;
	}

	public String getUsuaApellidos() {
		return usuaApellidos;
	}

	public void setUsuaApellidos(String usuaApellidos) {
		this.usuaApellidos = usuaApellidos;
	}

	public String getUsuaEmail() {
		return usuaEmail;
	}

	public void setUsuaEmail(String usuaEmail) {
		this.usuaEmail = usuaEmail;
	}

	public String getUsuaCelu() {
		return usuaCelu;
	}

	public void setUsuaCelu(String usuaCelu) {
		this.usuaCelu = usuaCelu;
	}

	public String getUsuaImagen() {
		return usuaImagen;
	}

	public void setUsuaImagen(String usuaImagen) {
		this.usuaImagen = usuaImagen;
	}

	public Date getUsuaFechnaci() {
		return usuaFechnaci;
	}

	public void setUsuaFechnaci(Date usuaFechnaci) {
		this.usuaFechnaci = usuaFechnaci;
	}

	public Character getUsuaGenero() {
		return usuaGenero;
	}

	public void setUsuaGenero(Character usuaGenero) {
		this.usuaGenero = usuaGenero;
	}

	public String getUsuaProveedor() {
		return usuaProveedor;
	}

	public void setUsuaProveedor(String usuaProveedor) {
		this.usuaProveedor = usuaProveedor;
	}

	public String getUsuaNacionalidad() {
		return usuaNacionalidad;
	}

	public void setUsuaNacionalidad(String usuaNacionalidad) {
		this.usuaNacionalidad = usuaNacionalidad;
	}

	public String getUsuaLuganaci() {
		return usuaLuganaci;
	}

	public void setUsuaLuganaci(String usuaLuganaci) {
		this.usuaLuganaci = usuaLuganaci;
	}

	public String getUsuaCivil() {
		return usuaCivil;
	}

	public void setUsuaCivil(String usuaCivil) {
		this.usuaCivil = usuaCivil;
	}

	public String getUsuaNiveeduc() {
		return usuaNiveeduc;
	}

	public void setUsuaNiveeduc(String usuaNiveeduc) {
		this.usuaNiveeduc = usuaNiveeduc;
	}

	public String getUsuaDiredomi() {
		return usuaDiredomi;
	}

	public void setUsuaDiredomi(String usuaDiredomi) {
		this.usuaDiredomi = usuaDiredomi;
	}

	public String getUsuaTeledomi() {
		return usuaTeledomi;
	}

	public void setUsuaTeledomi(String usuaTeledomi) {
		this.usuaTeledomi = usuaTeledomi;
	}

	public String getUsuaProfesion() {
		return usuaProfesion;
	}

	public void setUsuaProfesion(String usuaProfesion) {
		this.usuaProfesion = usuaProfesion;
	}

	public String getUsuaOcupasion() {
		return usuaOcupasion;
	}

	public void setUsuaOcupasion(String usuaOcupasion) {
		this.usuaOcupasion = usuaOcupasion;
	}

	public String getUsuaEmprtrab() {
		return usuaEmprtrab;
	}

	public void setUsuaEmprtrab(String usuaEmprtrab) {
		this.usuaEmprtrab = usuaEmprtrab;
	}

	public String getUsuaDiretrab() {
		return usuaDiretrab;
	}

	public void setUsuaDiretrab(String usuaDiretrab) {
		this.usuaDiretrab = usuaDiretrab;
	}

	public String getUsuaTeletrab() {
		return usuaTeletrab;
	}

	public void setUsuaTeletrab(String usuaTeletrab) {
		this.usuaTeletrab = usuaTeletrab;
	}

	public String getUsuaVivienda() {
		return usuaVivienda;
	}

	public void setUsuaVivienda(String usuaVivienda) {
		this.usuaVivienda = usuaVivienda;
	}

	public Character getUsuaEstado() {
		return usuaEstado;
	}

	public void setUsuaEstado(Character usuaEstado) {
		this.usuaEstado = usuaEstado;
	}

	public Date getUsuaCreacion() {
		return usuaCreacion;
	}

	public void setUsuaCreacion(Date usuaCreacion) {
		this.usuaCreacion = usuaCreacion;
	}

	@XmlTransient
	public List<EdPerfil> getEdPerfilList() {
		return edPerfilList;
	}

	public void setEdPerfilList(List<EdPerfil> edPerfilList) {
		this.edPerfilList = edPerfilList;
	}

	@XmlTransient
	public List<EdLibretacalificaciones> getEdLibretacalificacionesList() {
		return edLibretacalificacionesList;
	}

	public void setEdLibretacalificacionesList(List<EdLibretacalificaciones> edLibretacalificacionesList) {
		this.edLibretacalificacionesList = edLibretacalificacionesList;
	}

	@XmlTransient
	public List<EdMateriasporprofesor> getEdMateriasporprofesorList() {
		return edMateriasporprofesorList;
	}

	public void setEdMateriasporprofesorList(List<EdMateriasporprofesor> edMateriasporprofesorList) {
		this.edMateriasporprofesorList = edMateriasporprofesorList;
	}

	@XmlTransient
	public List<EdMatricula> getEdMatriculaList() {
		return edMatriculaList;
	}

	public void setEdMatriculaList(List<EdMatricula> edMatriculaList) {
		this.edMatriculaList = edMatriculaList;
	}

	@XmlTransient
	public List<EdCuadernillo> getEdCuadernilloList() {
		return edCuadernilloList;
	}

	public void setEdCuadernilloList(List<EdCuadernillo> edCuadernilloList) {
		this.edCuadernilloList = edCuadernilloList;
	}

	@XmlTransient
	public List<EdUsuainfo> getEdUsuainfoList() {
		return edUsuainfoList;
	}

	public void setEdUsuainfoList(List<EdUsuainfo> edUsuainfoList) {
		this.edUsuainfoList = edUsuainfoList;
	}

	@XmlTransient
	public List<EdFichaMedica> getEdFichamedicaList() {
		return edFichamedicaList;
	}

	public void setEdFichamedicaList(List<EdFichaMedica> edFichamedicaList) {
		this.edFichamedicaList = edFichamedicaList;
	}

	@XmlTransient
	public List<EdUsuario> getEdUsuarioList() {
		return edUsuarioList;
	}

	public void setEdUsuarioList(List<EdUsuario> edUsuarioList) {
		this.edUsuarioList = edUsuarioList;
	}

	public EdUsuario getEdUsuaId() {
		return edUsuaId;
	}

	public void setEdUsuaId(EdUsuario edUsuaId) {
		this.edUsuaId = edUsuaId;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (usuaId != null ? usuaId.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof EdUsuario)) {
			return false;
		}
		EdUsuario other = (EdUsuario) object;
		if ((this.usuaId == null && other.usuaId != null)
				|| (this.usuaId != null && !this.usuaId.equals(other.usuaId))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "prueba_dos.EdUsuario[ usuaId=" + usuaId + " ]";
	}

}
