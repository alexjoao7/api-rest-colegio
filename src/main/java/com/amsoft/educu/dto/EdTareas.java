package com.amsoft.educu.dto;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author JOAO
 */
@Entity
@Table(name = "ed_tareas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EdTareas.findAll", query = "SELECT e FROM EdTareas e")
    , @NamedQuery(name = "EdTareas.findByTareId", query = "SELECT e FROM EdTareas e WHERE e.tareId = :tareId")
    , @NamedQuery(name = "EdTareas.findByTareProfesor", query = "SELECT e FROM EdTareas e WHERE e.tareProfesor = :tareProfesor")
    , @NamedQuery(name = "EdTareas.findByTareDescripcion", query = "SELECT e FROM EdTareas e WHERE e.tareDescripcion = :tareDescripcion")
    , @NamedQuery(name = "EdTareas.findByTareAdjuarchivo", query = "SELECT e FROM EdTareas e WHERE e.tareAdjuarchivo = :tareAdjuarchivo")
    , @NamedQuery(name = "EdTareas.findByTareEnviada", query = "SELECT e FROM EdTareas e WHERE e.tareEnviada = :tareEnviada")
    , @NamedQuery(name = "EdTareas.findByTareEntrega", query = "SELECT e FROM EdTareas e WHERE e.tareEntrega = :tareEntrega")
    , @NamedQuery(name = "EdTareas.findByTareEstado", query = "SELECT e FROM EdTareas e WHERE e.tareEstado = :tareEstado")})
public class EdTareas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "tare_id")
    private Integer tareId;
    @Basic(optional = false)
    @Column(name = "tare_profesor")
    private String tareProfesor;
    @Basic(optional = false)
    @Column(name = "tare_descripcion")
    private String tareDescripcion;
    @Column(name = "tare_adjuarchivo")
    private String tareAdjuarchivo;
    @Basic(optional = false)
    @Column(name = "tare_enviada")
    @Temporal(TemporalType.DATE)
    private Date tareEnviada;
    @Basic(optional = false)
    @Column(name = "tare_entrega")
    @Temporal(TemporalType.DATE)
    private Date tareEntrega;
    @Basic(optional = false)
    @Column(name = "tare_estado")
    private Character tareEstado;
    @JoinColumn(name = "curso_id", referencedColumnName = "curso_id")
    @ManyToOne
    private EdCursos cursoId;
    @JoinColumn(name = "matemall_id", referencedColumnName = "matemall_id")
    @ManyToOne
    private EdMateriasmalla matemallId;

    public EdTareas() {
    }

    public EdTareas(Integer tareId) {
        this.tareId = tareId;
    }

    public EdTareas(Integer tareId, String tareProfesor, String tareDescripcion, Date tareEnviada, Date tareEntrega, Character tareEstado) {
        this.tareId = tareId;
        this.tareProfesor = tareProfesor;
        this.tareDescripcion = tareDescripcion;
        this.tareEnviada = tareEnviada;
        this.tareEntrega = tareEntrega;
        this.tareEstado = tareEstado;
    }

    public Integer getTareId() {
        return tareId;
    }

    public void setTareId(Integer tareId) {
        this.tareId = tareId;
    }

    public String getTareProfesor() {
        return tareProfesor;
    }

    public void setTareProfesor(String tareProfesor) {
        this.tareProfesor = tareProfesor;
    }

    public String getTareDescripcion() {
        return tareDescripcion;
    }

    public void setTareDescripcion(String tareDescripcion) {
        this.tareDescripcion = tareDescripcion;
    }

    public String getTareAdjuarchivo() {
        return tareAdjuarchivo;
    }

    public void setTareAdjuarchivo(String tareAdjuarchivo) {
        this.tareAdjuarchivo = tareAdjuarchivo;
    }

    public Date getTareEnviada() {
        return tareEnviada;
    }

    public void setTareEnviada(Date tareEnviada) {
        this.tareEnviada = tareEnviada;
    }

    public Date getTareEntrega() {
        return tareEntrega;
    }

    public void setTareEntrega(Date tareEntrega) {
        this.tareEntrega = tareEntrega;
    }

    public Character getTareEstado() {
        return tareEstado;
    }

    public void setTareEstado(Character tareEstado) {
        this.tareEstado = tareEstado;
    }

    public EdCursos getCursoId() {
        return cursoId;
    }

    public void setCursoId(EdCursos cursoId) {
        this.cursoId = cursoId;
    }

    public EdMateriasmalla getMatemallId() {
        return matemallId;
    }

    public void setMatemallId(EdMateriasmalla matemallId) {
        this.matemallId = matemallId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tareId != null ? tareId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EdTareas)) {
            return false;
        }
        EdTareas other = (EdTareas) object;
        if ((this.tareId == null && other.tareId != null) || (this.tareId != null && !this.tareId.equals(other.tareId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "prueba_dos.EdTareas[ tareId=" + tareId + " ]";
    }
    
}

