package com.amsoft.educu.dto;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author JOAO
 */
@Entity
@Table(name = "ed_perfil")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EdPerfil.findAll", query = "SELECT e FROM EdPerfil e")
    , @NamedQuery(name = "EdPerfil.findByPerfId", query = "SELECT e FROM EdPerfil e WHERE e.perfId = :perfId")
    , @NamedQuery(name = "EdPerfil.findByPerfDesc", query = "SELECT e FROM EdPerfil e WHERE e.perfDesc = :perfDesc")})
public class EdPerfil implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "perf_id")
    private Integer perfId;
    @Basic(optional = false)
    @Column(name = "perf_desc")
    private String perfDesc;
    @JoinTable(name = "ed_usuaperf", joinColumns = {
        @JoinColumn(name = "perf_id", referencedColumnName = "perf_id")}, inverseJoinColumns = {
        @JoinColumn(name = "usua_id", referencedColumnName = "usua_id")})
    @ManyToMany
    private List<EdUsuario> edUsuarioList;

    public EdPerfil() {
    }

    public EdPerfil(Integer perfId) {
        this.perfId = perfId;
    }

    public EdPerfil(Integer perfId, String perfDesc) {
        this.perfId = perfId;
        this.perfDesc = perfDesc;
    }

    public Integer getPerfId() {
        return perfId;
    }

    public void setPerfId(Integer perfId) {
        this.perfId = perfId;
    }

    public String getPerfDesc() {
        return perfDesc;
    }

    public void setPerfDesc(String perfDesc) {
        this.perfDesc = perfDesc;
    }

    @XmlTransient
    public List<EdUsuario> getEdUsuarioList() {
        return edUsuarioList;
    }

    public void setEdUsuarioList(List<EdUsuario> edUsuarioList) {
        this.edUsuarioList = edUsuarioList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (perfId != null ? perfId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EdPerfil)) {
            return false;
        }
        EdPerfil other = (EdPerfil) object;
        if ((this.perfId == null && other.perfId != null) || (this.perfId != null && !this.perfId.equals(other.perfId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "prueba_dos.EdPerfil[ perfId=" + perfId + " ]";
    }
    
}
