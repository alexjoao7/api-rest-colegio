package com.amsoft.educu.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author JOAO
 */
@Entity
@Table(name = "ed_libretacalificaciones")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EdLibretacalificaciones.findAll", query = "SELECT e FROM EdLibretacalificaciones e")
    , @NamedQuery(name = "EdLibretacalificaciones.findByLibrcaliId", query = "SELECT e FROM EdLibretacalificaciones e WHERE e.librcaliId = :librcaliId")
    , @NamedQuery(name = "EdLibretacalificaciones.findByLibrcaliParcuno", query = "SELECT e FROM EdLibretacalificaciones e WHERE e.librcaliParcuno = :librcaliParcuno")
    , @NamedQuery(name = "EdLibretacalificaciones.findByLibrcaliParcdos", query = "SELECT e FROM EdLibretacalificaciones e WHERE e.librcaliParcdos = :librcaliParcdos")
    , @NamedQuery(name = "EdLibretacalificaciones.findByLibrcaliParctres", query = "SELECT e FROM EdLibretacalificaciones e WHERE e.librcaliParctres = :librcaliParctres")
    , @NamedQuery(name = "EdLibretacalificaciones.findByLibrcaliQuimeuno", query = "SELECT e FROM EdLibretacalificaciones e WHERE e.librcaliQuimeuno = :librcaliQuimeuno")
    , @NamedQuery(name = "EdLibretacalificaciones.findByLibrcaliParccuat", query = "SELECT e FROM EdLibretacalificaciones e WHERE e.librcaliParccuat = :librcaliParccuat")
    , @NamedQuery(name = "EdLibretacalificaciones.findByLibrcaliParccinc", query = "SELECT e FROM EdLibretacalificaciones e WHERE e.librcaliParccinc = :librcaliParccinc")
    , @NamedQuery(name = "EdLibretacalificaciones.findByLibrcaliParcseis", query = "SELECT e FROM EdLibretacalificaciones e WHERE e.librcaliParcseis = :librcaliParcseis")
    , @NamedQuery(name = "EdLibretacalificaciones.findByLibrcaliQuimedos", query = "SELECT e FROM EdLibretacalificaciones e WHERE e.librcaliQuimedos = :librcaliQuimedos")
    , @NamedQuery(name = "EdLibretacalificaciones.findByLibrcaliPromfinal", query = "SELECT e FROM EdLibretacalificaciones e WHERE e.librcaliPromfinal = :librcaliPromfinal")
    , @NamedQuery(name = "EdLibretacalificaciones.findByNotaPromfinal", query = "SELECT e FROM EdLibretacalificaciones e WHERE e.notaPromfinal = :notaPromfinal")
    , @NamedQuery(name = "EdLibretacalificaciones.findByLibrcaliFechmodi", query = "SELECT e FROM EdLibretacalificaciones e WHERE e.librcaliFechmodi = :librcaliFechmodi")})
public class EdLibretacalificaciones implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "librcali_id")
    private Integer librcaliId;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "librcali_parcuno")
    private BigDecimal librcaliParcuno;
    @Column(name = "librcali_parcdos")
    private BigDecimal librcaliParcdos;
    @Column(name = "librcali_parctres")
    private BigDecimal librcaliParctres;
    @Column(name = "librcali_quimeuno")
    private BigDecimal librcaliQuimeuno;
    @Column(name = "librcali_parccuat")
    private BigDecimal librcaliParccuat;
    @Column(name = "librcali_parccinc")
    private BigDecimal librcaliParccinc;
    @Column(name = "librcali_parcseis")
    private BigDecimal librcaliParcseis;
    @Column(name = "librcali_quimedos")
    private BigDecimal librcaliQuimedos;
    @Column(name = "librcali_promfinal")
    private BigDecimal librcaliPromfinal;
    @Column(name = "nota_promfinal")
    private BigDecimal notaPromfinal;
    @Basic(optional = false)
    @Column(name = "librcali_fechmodi")
    @Temporal(TemporalType.DATE)
    private Date librcaliFechmodi;
    @JoinColumn(name = "usua_id", referencedColumnName = "usua_id")
    @ManyToOne
    private EdUsuario usuaId;

    public EdLibretacalificaciones() {
    }

    public EdLibretacalificaciones(Integer librcaliId) {
        this.librcaliId = librcaliId;
    }

    public EdLibretacalificaciones(Integer librcaliId, Date librcaliFechmodi) {
        this.librcaliId = librcaliId;
        this.librcaliFechmodi = librcaliFechmodi;
    }

    public Integer getLibrcaliId() {
        return librcaliId;
    }

    public void setLibrcaliId(Integer librcaliId) {
        this.librcaliId = librcaliId;
    }

    public BigDecimal getLibrcaliParcuno() {
        return librcaliParcuno;
    }

    public void setLibrcaliParcuno(BigDecimal librcaliParcuno) {
        this.librcaliParcuno = librcaliParcuno;
    }

    public BigDecimal getLibrcaliParcdos() {
        return librcaliParcdos;
    }

    public void setLibrcaliParcdos(BigDecimal librcaliParcdos) {
        this.librcaliParcdos = librcaliParcdos;
    }

    public BigDecimal getLibrcaliParctres() {
        return librcaliParctres;
    }

    public void setLibrcaliParctres(BigDecimal librcaliParctres) {
        this.librcaliParctres = librcaliParctres;
    }

    public BigDecimal getLibrcaliQuimeuno() {
        return librcaliQuimeuno;
    }

    public void setLibrcaliQuimeuno(BigDecimal librcaliQuimeuno) {
        this.librcaliQuimeuno = librcaliQuimeuno;
    }

    public BigDecimal getLibrcaliParccuat() {
        return librcaliParccuat;
    }

    public void setLibrcaliParccuat(BigDecimal librcaliParccuat) {
        this.librcaliParccuat = librcaliParccuat;
    }

    public BigDecimal getLibrcaliParccinc() {
        return librcaliParccinc;
    }

    public void setLibrcaliParccinc(BigDecimal librcaliParccinc) {
        this.librcaliParccinc = librcaliParccinc;
    }

    public BigDecimal getLibrcaliParcseis() {
        return librcaliParcseis;
    }

    public void setLibrcaliParcseis(BigDecimal librcaliParcseis) {
        this.librcaliParcseis = librcaliParcseis;
    }

    public BigDecimal getLibrcaliQuimedos() {
        return librcaliQuimedos;
    }

    public void setLibrcaliQuimedos(BigDecimal librcaliQuimedos) {
        this.librcaliQuimedos = librcaliQuimedos;
    }

    public BigDecimal getLibrcaliPromfinal() {
        return librcaliPromfinal;
    }

    public void setLibrcaliPromfinal(BigDecimal librcaliPromfinal) {
        this.librcaliPromfinal = librcaliPromfinal;
    }

    public BigDecimal getNotaPromfinal() {
        return notaPromfinal;
    }

    public void setNotaPromfinal(BigDecimal notaPromfinal) {
        this.notaPromfinal = notaPromfinal;
    }

    public Date getLibrcaliFechmodi() {
        return librcaliFechmodi;
    }

    public void setLibrcaliFechmodi(Date librcaliFechmodi) {
        this.librcaliFechmodi = librcaliFechmodi;
    }

    public EdUsuario getUsuaId() {
        return usuaId;
    }

    public void setUsuaId(EdUsuario usuaId) {
        this.usuaId = usuaId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (librcaliId != null ? librcaliId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EdLibretacalificaciones)) {
            return false;
        }
        EdLibretacalificaciones other = (EdLibretacalificaciones) object;
        if ((this.librcaliId == null && other.librcaliId != null) || (this.librcaliId != null && !this.librcaliId.equals(other.librcaliId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "prueba_dos.EdLibretacalificaciones[ librcaliId=" + librcaliId + " ]";
    }
}
