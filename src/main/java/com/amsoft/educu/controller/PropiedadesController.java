package com.amsoft.educu.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.amsoft.educu.dto.EdPropiedades;
import com.amsoft.educu.repository.IPropiedadesDao;

@RestController
@RequestMapping("/educu")
public class PropiedadesController {

	@Autowired
	IPropiedadesDao iPropiedadesDao;

	@GetMapping(path = "/propiedades")
	List<EdPropiedades> findAllProperties() {
		System.out.println("Consultando las propiedades");
		List<EdPropiedades> lstpropiedades = new ArrayList<EdPropiedades>();
		for (EdPropiedades propiedad : iPropiedadesDao.findAllProperties()) {
			lstpropiedades.add(propiedad);
		}
		return lstpropiedades;
	}

	@GetMapping(path = "/propiedades/{id}")
	List<EdPropiedades> getTypeProperties(@PathVariable Integer id) {
		List<EdPropiedades> lstpropiedades = new ArrayList<EdPropiedades>();
		for (EdPropiedades propiedad : iPropiedadesDao.findListProperties(id)) {
			lstpropiedades.add(propiedad);
		}
		return lstpropiedades;
	}

	@PostMapping(path = "/propiedades/create")
	String createProperties(@Valid @RequestBody EdPropiedades edPropiedad) {
		System.out.println("********************");
		System.out.println("1: " + edPropiedad.getPropEstado());
		System.out.println("2: " + edPropiedad.getPropNombre());
		System.out.println(edPropiedad);
		iPropiedadesDao.save(edPropiedad);
		return "Se ha guardado correctamente la nueva propiedad";
	}

	@PostMapping(path = "/propiedades/delete/{id}")
	String deleteProperties(@PathVariable Integer id) {
		iPropiedadesDao.deleteById(id);
		return "Se ha eliminado correctamente la propiedad";
	}

	@PostMapping(path = "/propiedades/update/{id}")
	public ResponseEntity<String> updateProperties(@PathVariable Integer id, @RequestHeader String propNombre,
			@RequestHeader String propEstado) {
		EdPropiedades edpropiedad = iPropiedadesDao.findById(id).get();
		edpropiedad.setPropNombre(propNombre);
		edpropiedad.setPropEstado(propEstado.charAt(0));
		iPropiedadesDao.save(edpropiedad);
		return new ResponseEntity<String>(String.format("Se ha actualizado correctamente la propiedad", "Mensaje"),
				HttpStatus.OK);
	}

}
