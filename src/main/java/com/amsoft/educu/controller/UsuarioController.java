package com.amsoft.educu.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.amsoft.educu.dto.EdUsuario;
import com.amsoft.educu.repository.IUsuarioDao;

@RestController
@RequestMapping("/educu")
public class UsuarioController {

	@Autowired
	private IUsuarioDao usuariodao;

	@GetMapping(path = "/usuarios")
	List<EdUsuario> findAll() {
		List<EdUsuario> lstusuarios = new ArrayList<EdUsuario>();
		for (EdUsuario usuario : usuariodao.findAll()) {
			lstusuarios.add(usuario);
		}
		return lstusuarios;
	}

	@GetMapping(path = "/usuarios/{id}")
	EdUsuario findByUser(@PathVariable Integer id) {
		EdUsuario usuario = null;
		Optional<EdUsuario> usuarioOptional = usuariodao.findById(id);
		if (usuarioOptional.isPresent()) {
			usuario = usuarioOptional.get();
		}
		return usuario;
	}

	@PostMapping(path = "/usuarios/create")
	String createUser(@RequestBody EdUsuario edUsuario) {
		System.out.println("Usted esta tratando de ingresar un nuevo cliente");
		usuariodao.save(edUsuario);
		return "Se ha guardado correctamente el nuevo usuario";
	}

	@PostMapping(path = "/usuarios/delete/{id}")
	String deleteUser(@PathVariable Integer id) {
		usuariodao.deleteById(id);
		return "Se ha eliminado correctamente el usuario";
	}

	@PostMapping(path = "/usuarios/update/{id}")
	public ResponseEntity<String> updateUser(@PathVariable Integer id, @RequestHeader String password) {
		EdUsuario edusuario = usuariodao.findById(id).get();
		// edusuario.setUsuaPassword(password);
		usuariodao.save(edusuario);
		return new ResponseEntity<String>(String.format("Se ha actualizado correctamente el usuario", "Mensaje"),
				HttpStatus.OK);
	}

}
