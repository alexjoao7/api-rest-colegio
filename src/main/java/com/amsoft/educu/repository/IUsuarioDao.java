package com.amsoft.educu.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.amsoft.educu.dto.EdUsuario;

@Repository
public interface IUsuarioDao extends JpaRepository<EdUsuario, Integer> {

	@Query(value = "SELECT * FROM ed_usuario u WHERE u.usua_name=:name AND u.usua_passtemp=:password AND u.usua_estado='1'", nativeQuery = true)
	public abstract EdUsuario findByUser(@Param("name") String name, @Param("password") String password);

}
