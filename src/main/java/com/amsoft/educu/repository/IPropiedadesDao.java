package com.amsoft.educu.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.amsoft.educu.dto.EdPropiedades;

public interface IPropiedadesDao extends JpaRepository<EdPropiedades, Integer> {

	@Query(value = "SELECT * FROM ed_propiedades p WHERE p.ed__prop_id is null AND p.prop_estado='1'", nativeQuery = true)
	public abstract List<EdPropiedades> findAllProperties();

	@Query(value = "SELECT * FROM ed_propiedades p WHERE p.ed__prop_id=:id AND p.prop_estado='1'", nativeQuery = true)
	public abstract List<EdPropiedades> findListProperties(@Param("id") Integer id);
}
