package com.amsoft.educu.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.amsoft.educu.dto.EdLogin;


public interface ILoginDao extends JpaRepository<EdLogin, Integer> {

	@Query(value = "SELECT * FROM ed_login l WHERE l.logi_usuario=:usuario AND l.logi_password=:password AND l.logi_estado='1'", nativeQuery = true)
	public abstract EdLogin findByUser(@Param("usuario") String usuario, @Param("password") String password);
}
