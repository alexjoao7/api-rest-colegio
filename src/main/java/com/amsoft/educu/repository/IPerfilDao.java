package com.amsoft.educu.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.amsoft.educu.dto.EdPerfil;

public interface IPerfilDao extends JpaRepository<EdPerfil, String> {

}
